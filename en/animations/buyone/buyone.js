
function Buyone(resources)
{
	Buyone.resources = resources;
}
Buyone.prototype = {
	init: function()
	{
		this.game = new Phaser.Game(800, 400, Phaser.CANVAS, 'buyone', { preload: this.preload, create: this.create, update: this.update, render: this.render,parent:this});


	},

	preload: function()
	{
		this.game.stage.backgroundColor = "#ffffff";
        this.game.scale.maxWidth = 800;
    	this.game.scale.maxHeight = 400;
		this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
		this.game.load.image('image_1', Buyone.resources.image_1);
		this.game.load.image('image_2', Buyone.resources.image_2);
		this.game.load.image('image_3', Buyone.resources.image_3);
		this.game.load.image('image_4', Buyone.resources.image_4);
    },

	create: function(evt)
	{
		this.parent.buildAnimation();
  	},

	buildAnimation: function()
	{
	for( var i = 0;i<4;i++)
	{
		var cnt = i+1;
		this["sprite_"+cnt] = this.game.add.sprite(-450, 240, 'image_'+cnt);
		this["sprite_"+cnt].anchor.set(0.5);
		this["sprite_"+cnt].smoothed = false;
		this["spriteTween_"+cnt]= this.game.add.tween(this["sprite_"+cnt]).to( { x: 105+(200*i)}, 80, Phaser.Easing.Linear.InOut);
		this["text_"+cnt] = this.game.add.text(-455, 380,Buyone.resources["text_"+cnt],Buyone.resources.style_1);
		this["text_"+cnt].anchor.set(0.5);
		this["text_"+cnt].smoothed = false;
		this["textTween_"+cnt] = this.game.add.tween(this["text_"+cnt]).to( { x: 105+(200*i) }, 50, Phaser.Easing.Linear.Out);
	}
	this.spriteTween_1.chain(this.textTween_1,this.spriteTween_2,this.textTween_2,this.spriteTween_3,this.textTween_3,this.spriteTween_4,this.textTween_4);
	this.spriteTween_1.start();

	},
	update: function()
	{

	},

	render: function()
	{

	}

}